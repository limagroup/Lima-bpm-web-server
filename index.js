import 'bootstrap/dist/css/bootstrap.css';
import React from 'react';
import ReactDom from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { Router, Switch, Route } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { createLogger } from 'redux-logger';
import { composeWithDevTools } from 'redux-devtools-extension';

import rootReducer from './ui/reducers/index';
import List from './ui/components/list';
import Main from './ui/containers/main';

const history = createBrowserHistory();

const store=createStore(rootReducer,
  composeWithDevTools(
    applyMiddleware(
      thunk
    )
  )
);



ReactDom.render(
  <Provider store={store}>
    <Router history={history}>
      <Switch>
        <Route exact path="/:bpmKey" component={Main} />
        <Route exact path="/" component={List} />
      </Switch>
    </Router>
  </Provider>,
  document.getElementById('main')
);
