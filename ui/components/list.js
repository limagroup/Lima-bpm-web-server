import React, { Component } from 'react';
import { Container, Navbar } from 'react-bootstrap';
import List from '../containers/list'

export default class ListContainer extends Component {
  render() {
    return (
        <div>
        <Navbar bg="dark" variant="dark" className="mb-2">
          <Navbar.Brand>Cameras</Navbar.Brand>
        </Navbar>
        <List />
      </div>
    )
  }
}
