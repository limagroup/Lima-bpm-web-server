import React from 'react';
import { Container, Col, Row, Alert } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { hideAlert } from '../actions/canvas.js'
import { Badge } from 'react-bootstrap'

class Infos extends React.Component {
  constructor(props) {
    super(props);

    this.closeClicked = this.closeClicked.bind(this);
  }

  // Message is now closed after new image. No need of this but still keeping it just in case.
  closeClicked(){
    this.props.hideAlert();
  }

  render(){
    const showWarning = this.props.beam_markX > this.props.cameraFullWidth
      || this.props.beam_markY > this.props.cameraFullHeight;

    return (
      <Container>
        <Row>
          <Col>
            <h5>
              <Badge variant="info">
                Frame <Badge variant="light">{this.props.img_num}</Badge>
              </Badge>
            </h5>
          </Col>
          <Col>
            <h5>
              <Badge variant="info">
                FWHM <Badge variant="light">{Math.round(this.props.fwhmX*100)/100} x {Math.round(this.props.fwhmY*100)/100}</Badge>
              </Badge>
            </h5>
          </Col>
          <Col>
            <h5>
              <Badge variant="info">
                Intensity <Badge variant="light">{this.props.intensity}</Badge>
              </Badge>
            </h5>
          </Col>
          <Col>
            <h5>
              <Badge variant="info">
                Beam <Badge variant="light">bx={Math.round(this.props.bx)} by= {Math.round(this.props.by)}</Badge>
              </Badge>
            </h5>
          </Col>
          <Col></Col>
        </Row>

        <Row hidden={!this.props.activeROI}>
          <Col>
            <h5>
              <Badge variant="info">
                ROI
                <Badge variant="light" className="ml-1 mr-1">x={Math.round(this.props.start_X_display*this.props.calib_x)}</Badge>
                <Badge variant="light" className="mr-1">y={Math.round(this.props.start_Y_display*this.props.calib_y)}</Badge>
                <Badge variant="light" className="mr-1">w={Math.round(this.props.width*this.props.calib_x)}</Badge>
                <Badge variant="light">h={Math.round(this.props.height*this.props.calib_y)}</Badge>
              </Badge>
            </h5>
          </Col>
        </Row>

        <Row>
          <Col>
            <Alert variant="success" hidden={this.props.alertHidden}>ROI is applied/reset successfully. Acquiring new image</Alert>
            <Alert variant="warning" hidden={!showWarning}>The beam mark is outside the current image dimensions</Alert>
          </Col>
        </Row>
      </Container>
   );

}

}

function mapStateToProps(state) {
  return {
    img_num : state.canvas.img_num,
    fwhmX :state.canvas.fwhmX,
    fwhmY :state.canvas.fwhmY,
    intensity :state.canvas.intensity,
    bx :state.canvas.bx,
    by :state.canvas.by,
    alertHidden:state.canvas.alertHidden,
    start_X_display: state.canvas.start_X_display,
    start_Y_display: state.canvas.start_Y_display,
    width: state.canvas.width,
    height: state.canvas.height,
    activeROI:state.video.activeROI,
    calib_x:state.options.calib_x,
    calib_y:state.options.calib_y,
    beam_markX: state.canvas.beam_markX,
    beam_markY: state.canvas.beam_markY,
    cameraFullWidth: state.canvas.cameraFullWidth,
    cameraFullHeight: state.canvas.cameraFullHeight,
  };
}

 function mapDispatchToProps(dispatch) {
  return {
    hideAlert:bindActionCreators(hideAlert,dispatch)

  };
}


export default connect(mapStateToProps,mapDispatchToProps)(Infos);
