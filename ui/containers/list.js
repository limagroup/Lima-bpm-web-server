import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { Container, Badge, ListGroup } from 'react-bootstrap';
import { getList } from '../actions/navbar';

class List extends Component {

  constructor(props) {
    super(props)
    this.props.getList()
    this.interval = setInterval(this.props.getList, 5000)
  }

  componentDidMount() {
    document.title = "BPM Monitor"
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  render() {
    return (
      <Container>
        <ListGroup>
          {this.props.bpmList.map((item, key) => 
            <ListGroup.Item key={item.bpm}>
              <Badge variant={item.status?'success':'secondary'}>{item.status?'Online':'Offline'}</Badge> 
              &nbsp;
              {item.name}
              &nbsp;
              { item.status === 1 && 
                <React.Fragment>
                  [<Link to={`/${item.name}`}>View</Link>]
                </React.Fragment>
              }
            </ListGroup.Item>
          )}
        </ListGroup>
        { !this.props.bpmListFetched &&
          <p>Waiting for device list</p>
        }
        { this.props.bpmList.length === 0 &&
          <p>No devices found</p>
        }
      </Container>
    )
  }
}


function mapStateToProps(state) {
  return {
    bpmList: state.bpmState.bpmList,
    bpmListFetched: state.bpmState.bpmListFetched
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getList: bindActionCreators(getList, dispatch),
  };
}

export default connect(mapStateToProps,mapDispatchToProps)(List);
