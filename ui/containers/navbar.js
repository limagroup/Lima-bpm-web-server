import React from 'react';
import { Navbar, Badge, NavDropdown } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { getStatus, clearImage } from '../actions/canvas.js';

import List from './list';

class NavBar extends React.Component {
  getStatus(){
    this.props.getStatus(window.innerWidth,window.innerHeight);
  }


  render(){
    return (
      <Navbar bg="dark" variant="dark" className="mb-2">
        <Navbar.Brand><Link to="/">Cameras</Link></Navbar.Brand>
        <NavDropdown title="List" id="list-dropdown" className="mr-auto">
          <List />
        </NavDropdown>
        <h4>
          <Badge variant={this.props.status?'success':'danger'} className="mr-2">{this.props.status?'Online':'Offline'}</Badge>
          <Badge variant="light">{this.props.client_id}</Badge>
        </h4>
      </Navbar>
    );
  }

  componentDidMount() {
    this.getStatus();
  }

  componentDidUpdate(prevProps) {
    if (this.props.client_id != prevProps.client_id ||
      (this.props.status != prevProps.status && this.props.status == 1)
      ) {
      this.getStatus();
      this.props.clearImage();
    }
  }

}

function mapStateToProps(state) {
  let status = 0
  state.bpmState.bpmList.forEach((bpm) => {
    if (bpm.name == state.bpmState.client_id) {
      status = bpm.status
    }
  })

  return {
    client_id: state.bpmState.client_id,
    status: status,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getStatus: bindActionCreators(getStatus, dispatch),
    clearImage: bindActionCreators(clearImage, dispatch),
  };
}

export default connect(mapStateToProps,mapDispatchToProps)(NavBar);
