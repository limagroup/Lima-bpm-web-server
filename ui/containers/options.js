import React from 'react';
import { Form,Col,Button,Container,Row,Badge } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import config from '../config';
import { latencyTimeNotInRange, exposureTimeNotInRange, buttonAcquirePressed, textEnterExposure, textEnterSampling, textEmptyExposure, textEmptySampling, liveChecked, setLiveChecked, textEnterCalib_X, textEnterCalib_Y, textEmptyX, textEmptyY, setImgDisplay, getImgDisplay, update_calibration_apply, setAcqStatus, stopAcq, lockToggle} from '../actions/options.js'
import Dropdown from '../components/dropdown.js'

class Options extends React.Component {
  constructor(props) {
    super(props);
    this.buttonAcquirePressed = this.buttonAcquirePressed.bind(this);
    this.textStateExposure = this.textStateExposure.bind(this);
    this.textStateSampling = this.textStateSampling.bind(this);
    this.textStateCalib_x = this.textStateCalib_x.bind(this);
    this.textStateCalib_y = this.textStateCalib_y.bind(this);

    this.initSocket()

    this.localUpdate = false

    this.exposureRef = React.createRef();
    this.samplingRef = React.createRef();
  }

  initSocket() {
    let rt = config.base_url.replace('http://', '') || (window.location.hostname+':'+window.location.port)

    this.ws = new WebSocket(`ws://${rt}/socket`);
    this.ws.onopen = function() {
        console.log('websocket connected')
    }
    var self = this
    this.ws.onmessage = function (evt) {
        const data = JSON.parse(evt.data)
        if (self.props.client_id === data.camera_name) {
          if (data.event === 'bvdata') self.props.getImgDisplay()
          if (data.event === 'video_live') self.props.setLiveChecked(data.value?1:0)
          if (data.event === 'acq_status') self.props.setAcqStatus(data.value)
          if (data.event === 'acq_expo_time') {
            self.exposureRef.current.value = data.value
            self.props.textEnterExposure(data.value)
          }
        }
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.exposureTimeValue !== this.props.exposureTimeValue && prevProps.samplingRateValue !== this.props.samplingRateValue) {
      this.samplingRef.current.value = this.props.samplingRateValue
      this.exposureRef.current.value = this.props.exposureTimeValue
      return
    }

    if (prevProps.exposureTimeValue !== this.props.exposureTimeValue && !this.localUpdate) {
      if (!this.props.lockExposureBool) return
      if (!this.props.exposureTimeValue) return

      this.localUpdate = true
      const newSampling = 1/(this.props.exposureTimeValue+(1.000001*this.props.minLatencyTime));
      this.props.textEnterSampling(newSampling);
      this.samplingRef.current.value = newSampling.toFixed(6);
      setTimeout(() => this.localUpdate = false, 0);
    }

    if (prevProps.samplingRateValue !== this.props.samplingRateValue && !this.localUpdate) {
      if (!this.props.lockExposureBool) return
      if (!this.props.samplingRateValue) return
      this.localUpdate = true
      const newExposure = (1/(this.props.samplingRateValue))-(1.00001*this.props.minLatencyTime);
      this.props.textEnterExposure(newExposure);
      this.exposureRef.current.value = newExposure.toFixed(6);
      setTimeout(() => this.localUpdate = false, 0);
    }
  }


  textStateExposure(evt){
    const val = parseFloat(evt.target.value)
    if (evt.target.value > -0.0001){ // if >0, can't write "0,xxx" because of the first 0
      this.props.textEnterExposure(isNaN(val) ? '' : val);
    } else {
      this.props.textEmptyExposure(); 
    }
  }

  textStateSampling(evt){
    const val = parseFloat(evt.target.value)
    if(evt.target.value > -0.0001){
      this.props.textEnterSampling(isNaN(val) ? '' : val);
    } //else {
      // this.props.textEmptySampling();
    // }
  }

  textStateCalib_x(evt) {
    if(evt.target.value > 0) {
      this.props.textEnterCalib_X(parseFloat(evt.target.value));
    } else {
      this.props.textEmptyX();
    }
  }


  textStateCalib_y(evt) {
    if(evt.target.value > 0) {
      this.props.textEnterCalib_Y(parseFloat(evt.target.value));
    } else {
      this.props.textEmptyY();
    }
  }

  buttonAcquirePressed(){
    if (this.props.acqStatus == 'Ready') {
      if (this.props.exposureTimeValue > this.props.maxExposureTime || this.props.exposureTimeValue < this.props.minExposureTime) {
        this.props.exposureTimeNotInRange();

      } else {
        if ((1/this.props.samplingRateValue)-this.props.exposureTimeValue > this.props.maxLatencyTime || 
            (1/this.props.samplingRateValue)-this.props.exposureTimeValue < this.props.minLatencyTime) {
          this.props.latencyTimeNotInRange();

        } else {
          this.props.setImgDisplay();
        }
      }
    } else {
      this.props.stopAcq();
    }
    // console.log((1/this.props.samplingRateValue)-this.props.exposureTimeValue)
  }


  render(){
    const ready = this.props.acqStatus === 'Ready'
    const text = ready ? 'Acquire' : 'Stop'
    const variant = ready ? 'success' : 'danger'

    return (
      <Container>
        <Row>
          <Col>
            <Form.Group as={Row}>
              <Form.Label column>Exposure time (s):</Form.Label>
              <Col>
                <Form.Control type="number" ref={this.exposureRef} defaultValue={this.props.exposureTimeValue} placeholder="Value" onChange={this.textStateExposure} readOnly={!ready} />
              </Col>
            </Form.Group>

            <Row className="ml-auto">
              <Form.Check type="checkbox" onChange={this.props.lockToggle} checked={this.props.lockExposureBool === true} label="Linked" id="lock-check" />
            </Row>

            <Form.Group as={Row}>
              <Form.Label column>Sampling rate (Hz):</Form.Label>
              <Col>
                <Form.Control type="number" defaultValue={this.props.samplingRateValue} ref={this.samplingRef} placeholder="Value" onChange={this.textStateSampling} readOnly={!ready} />
              </Col>
            </Form.Group>
          </Col>

          <Col>
            <Button variant={variant} onClick={this.buttonAcquirePressed}>{text}</Button>

            <Form.Check type="checkbox" onChange={this.props.liveChecked} checked={this.props.liveCheckedBool === 1} disabled={!ready} label="Live" id="live-check" />

            <Badge variant={this.props.acqStatus==='Ready'?'success':'danger'}>{this.props.acqStatus}</Badge>
          </Col>

          <Col>
            <Dropdown title="Pixel Size">
              <Container>
                <Form.Group as={Row}>
                  <Form.Label column>X: </Form.Label>
                  <Col>
                    <Form.Control type="number" value={this.props.calib_x} onChange={this.textStateCalib_x} placeholder="Value required" readOnly={!ready} /> &micro;m
                  </Col>
                </Form.Group>
                <Form.Group as={Row}>
                  <Form.Label column>Y: </Form.Label>
                  <Col>
                    <Form.Control type="number" value={this.props.calib_y} onChange={this.textStateCalib_y} placeholder="Value required" readOnly={!ready} /> &micro;m
                  </Col>
                </Form.Group>
              
                <Button disabled={this.props.applyDisabled || !ready } onClick={this.props.update_calibration_apply} >Apply</Button>
              </Container>
            </Dropdown>
          </Col>
        </Row>
      </Container>
   );

  }

}

function mapStateToProps(state) {
  return {
    buttonAcquireStyle: state.options.buttonAcquireStyle,
    buttonAcquireText: state.options.buttonAcquireText,
    buttonAcquireGlyphiconText: state.options.buttonAcquireGlyphiconText,
    exposureTimeValue: state.options.exposureTimeValue,
    samplingRateValue: state.options.samplingRateValue,
    liveCheckedBool:state.options.liveCheckedBool,
    acqStatus:state.options.acqStatus,
    calib_x: state.options.calib_x,
    calib_y: state.options.calib_y,
    applyDisabled:state.options.applyDisabled,
    minExposureTime:state.options.minExposureTime,
    maxExposureTime:state.options.maxExposureTime,
    minLatencyTime : state.options.minLatencyTime,
    maxLatencyTime : state.options.maxLatencyTime,
    client_id: state.bpmState.client_id,
    lockExposureBool: state.options.lockExposure,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    buttonAcquirePressed: bindActionCreators(buttonAcquirePressed, dispatch),
    textEnterExposure: bindActionCreators(textEnterExposure, dispatch),
    textEnterSampling: bindActionCreators(textEnterSampling, dispatch),
    textEmptyExposure: bindActionCreators(textEmptyExposure, dispatch),
    textEmptySampling: bindActionCreators(textEmptySampling, dispatch),
    liveChecked:bindActionCreators(liveChecked, dispatch),
    setLiveChecked:bindActionCreators(setLiveChecked, dispatch),
    setAcqStatus: bindActionCreators(setAcqStatus, dispatch),
    stopAcq: bindActionCreators(stopAcq, dispatch),
    textEnterCalib_X:bindActionCreators(textEnterCalib_X, dispatch),
    textEnterCalib_Y:bindActionCreators(textEnterCalib_Y, dispatch),
    textEmptyX:bindActionCreators(textEmptyX, dispatch),
    textEmptyY:bindActionCreators(textEmptyY, dispatch),
    setImgDisplay:bindActionCreators(setImgDisplay,dispatch),
    getImgDisplay:bindActionCreators(getImgDisplay,dispatch),
    update_calibration_apply:bindActionCreators(update_calibration_apply,dispatch),
    latencyTimeNotInRange:bindActionCreators(latencyTimeNotInRange,dispatch),
    exposureTimeNotInRange:bindActionCreators(exposureTimeNotInRange,dispatch),
    lockToggle:bindActionCreators(lockToggle,dispatch),
  };
}

export default connect(mapStateToProps,mapDispatchToProps)(Options);
