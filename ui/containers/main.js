import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import NavBar from './navbar';
import Options from './options';
import Infos from '../components/infos';
import Video from './video';
import { setClientId } from '../actions/navbar';

class Main extends Component {
  constructor(props) {
    super(props)
    this.setClientId()
  }

  setClientId() {
    this.props.setClientId(this.props.match.params.bpmKey)
  }

  componentDidMount() {
    document.title = `${this.props.match.params.bpmKey} » BPM Monitor`
  }

  componentDidUpdate(prevProps) {
    this.setClientId()
  }
  
  render(){
    return (
      <div>
        <NavBar />
        <Options />
        <Infos />
        <Video />
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    bpmList:state.bpmList
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setClientId: bindActionCreators(setClientId, dispatch),
  };
}

export default connect(mapStateToProps,mapDispatchToProps)(Main);
