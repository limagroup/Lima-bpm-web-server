import React from 'react';
import { Container,Row,Col,Dropdown,Button,DropdownButton,ButtonGroup,FormCheck,DropdownItem,OverlayTrigger,Tooltip,SplitButton,Form,ToggleButton,ToggleButtonGroup } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {linearClicked,logarithmicClicked,autoscaleChecked,temperatureChecked,updateBackground,setRoi,setRotation,setFlip, setCrosshair, setLockCrosshair, setOptions,updateMin,updateMax,setProfileType } from '../actions/video.js'
import {resetRoi, getStatus, profileSize, updateDimensions,setBeamMark,setBeamMarkDone } from '../actions/canvas.js'
import {setImgDisplay,setLiveChecked} from '../actions/options.js'
import Canvas from '../components/canvas.js'
import Dygraph from 'dygraphs'
import classNames from 'classnames'

class Video extends React.Component {
  constructor(props) {
    super(props);
    this.resetROI = this.resetROI.bind(this);
    this.resetCROSSHAIR = this.resetCROSSHAIR.bind(this);
    this.rotation = this.rotation.bind(this);
    this.graph = this.graph.bind(this);
    this.updateMin = this.updateMin.bind(this);
    this.updateMax = this.updateMax.bind(this);
    this.saveMinMax = this.saveMinMax.bind(this);
    this.resetMinMax = this.resetMinMax.bind(this);

    this.graph_profilX = undefined;
    this.graph_profilY = undefined;

    this.graph1 = React.createRef()
    this.graph2 = React.createRef()

    this.divider = 10
  }


  componentDidUpdate(prevProps) {
    if ((this.props.profileX.length != 0 && this.props.profileY.length != 0) ||
        (this.props.profileSize != prevProps.profileSize) ||
        (this.props.windowWidth != prevProps.windowWidth || this.props.windowHeight != prevProps.windowHeight)
      ) {
      this.graph(prevProps);
    }

  }

  resetCROSSHAIR() {
    return this.props.setBeamMark(0,0).then(() => {
      return this.props.setCrosshair()
    })
  }

  resetROI() {
    this.props.resetRoi().then(() => {
        this.props.getStatus(window.innerWidth,window.innerHeight).then(() => {
          if (this.props.activeBkgnd===true) { //if there is a bkg when we reset ROI, then we need to reset it because of changes in dimensions.
            this.props.updateBackground();
          }
          this.props.setLiveChecked(0);
          this.props.setImgDisplay();
        })
    })
  }

  rotation(rotation) {
    this.props.setRotation(rotation).then(() => {
      this.props.getStatus(window.innerWidth,window.innerHeight).then(() => {
        this.props.setLiveChecked(0);
        this.props.setImgDisplay();
      })
    })
  }

  flip(axis) {
    let flip = [...this.props.flip]
    flip[axis] = !flip[axis]

    this.props.setFlip(flip)
  }

  graph(prevProps) {
    if(this.graph_profilX===undefined && this.graph_profilY===undefined){
      var data = [];
      for(let i=0;i <= this.props.profileX.length;i++){
        data.push([i*this.props.calib_x, this.props.profileX[i]]);
      }
      this.graph_profilX = new Dygraph(this.graph1.current, data, {
          drawPoints: false, 
          showRoller: false, 
          legend: 'never',
          labels: ['Profile_X', 'Intensity'], 
          dateWindow: [0, this.props.profileX.length*this.props.calib_x], 
          width: this.props.windowWidth, 
          height: this.props.windowHeight/this.divider*this.props.profileSize.x,
          axes: {
            y: {
              drawAxis: false
            }
          },
          zoomCallback: () => {
            this.graph_profilX.updateOptions({ valueRange: null, dateWindow: [0, this.props.profileX.length] })
            return
          }
        }
      );
    
      var data2 = [];
      for(let i=0; i < this.props.profileY.length; i++){
        data2.push([this.props.profileY[i], i*this.props.calib_y])
      }
      this.graph_profilY = new Dygraph(this.graph2.current, data2, {
          drawPoints: false, 
          showRoller: false, 
          legend: 'never',
          labels: ['Intensity', 'Profile_Y'], 
          dateWindow : [Math.min(...this.props.profileY),Math.max(...this.props.profileY)], 
          valueRange: [this.props.profileY.length*this.props.calib_y, 0],
          width: this.props.windowHeight/this.divider*this.props.profileSize.y+50,
          height: this.props.windowHeight,
          axes: {
            x: {
              drawAxis: false,
            }
          },
          zoomCallback: () => {
            this.graph_profilY.updateOptions({ valueRange: [0, this.props.profileY.length], dateWindow: [Math.min(...this.props.profileY),Math.max(...this.props.profileY)] })
            return
          }
        }
      );

    } else {
      var data = [];
      for(let i=0;i <= this.props.profileX.length;i++){
        data.push([i*this.props.calib_x, this.props.profileX[i]]);
      }

      this.graph_profilX.updateOptions({
        file: data,
        dateWindow: [0, this.props.profileX.length*this.props.calib_x],
        axes: {
          y: {
            drawAxis: this.props.profileSize.x === 3,
          }
        },
      })
      this.graph_profilX.resize(this.props.windowWidth, 
        this.props.windowHeight/this.divider*this.props.profileSize.x)
      
      var data2 = [];
      for(let i=0; i < this.props.profileY.length; i++){
        data2.push([this.props.profileY[i],i*this.props.calib_y])
      }
      this.graph_profilY.updateOptions({
        file: data2,
        dateWindow: [Math.min(...this.props.profileY),Math.max(...this.props.profileY)],
        valueRange: [this.props.profileY.length*this.props.calib_y, 0],
        axes: {
          x: {
            drawAxis: this.props.profileSize.y === 3,
          }
        },
      })
      this.graph_profilY.resize(this.props.windowHeight/this.divider*this.props.profileSize.y+50, 
        this.props.windowHeight)
    }
  }

  
  toggleProfileSize(axis, evt) {
    this.props.setProfileSize(axis, this.props.profileSize[axis] === 3 ? 1 : 3)
  }

  toggleHideProfile() {
    // inverted because this fires before the next render
    let scale = this.props.profileSize['x'] === 1 ? 1.3 : 1
    this.props.updateDimensions(window.innerWidth*scale, window.innerHeight*scale)

    this.props.setProfileSize('x', this.props.profileSize['x'] === 0  ? 1 : 0)
  }

  lockCrosshair() {
      this.props.setLockCrosshair(!this.props.lockCrosshair)
  }

  updateMin(e) {
    this.props.updateMin(parseInt(e.target.value))
  }

  updateMax(e) {
    this.props.updateMax(parseInt(e.target.value))
  }

  saveMinMax() {
    this.props.setOptions({ min_max: this.props.min_max }).then(() => {
      this.props.getStatus(window.innerWidth, window.innerHeight)
    })
  }

  resetMinMax() {
    this.props.setOptions({ min_max: [0,0] })
  }

  render() {
    const crosshair = (
      <Tooltip id="tooltip">Click here to set/lock crosshair data: X={this.props.beam_markX} Y={this.props.beam_markY}</Tooltip>
    );

    const background = (
      <Tooltip id="tooltip">Click here to take/reset background</Tooltip>
    );

    const roi = (
      <Tooltip id="tooltip">Click here to activate/deactivate ROI mode</Tooltip>
    );

    const rotation = (
      <Tooltip id="tooltip">Click here to rotate the image in steps of 90 degrees and/or flip</Tooltip>
    );

    const scale = (
      <Tooltip id="tooltip">Click here to change the image scaling</Tooltip>
    );

    const profiles = (
      <Tooltip id="tooltip">Click here to show / hide image profiles</Tooltip>
    );

    const profileType = (
      <Tooltip id="tooltip">Show profiles for beam or crosshair position</Tooltip>
    );

    const ready = this.props.acqStatus == 'Ready';

    return (  
      <Container>
        <Row className="mb-1">
          <Col>
            <ButtonGroup className="mr-2">
              <OverlayTrigger placement="top" overlay={crosshair}>
                <SplitButton title="Lock Crosshair" onClick={this.lockCrosshair.bind(this)} disabled={0} variant={this.props.lockCrosshair?'success':'primary'}>
                  <DropdownItem onClick={this.resetCROSSHAIR} disabled={0}>Reset </DropdownItem>
                </SplitButton>
              </OverlayTrigger>

              <OverlayTrigger placement="top" overlay={background}>
                <Button onClick={this.props.updateBackground} variant={this.props.activeBkgnd?'success':'primary'} disabled={!ready}> Background</Button>
              </OverlayTrigger>

              <OverlayTrigger placement="top" overlay={roi}>
                <SplitButton title="ROI" onClick={this.props.setRoi} disabled={!ready} variant={this.props.activeROI?'secondary':(this.props.activeROIServer?'success':'primary')}>
                <DropdownItem onClick={this.resetROI} disabled={!ready || this.props.resetDesactivated}>Reset</DropdownItem>
              </SplitButton>
              </OverlayTrigger>

              <OverlayTrigger placement="top" overlay={scale}>
                <DropdownButton as={ButtonGroup} title={this.props.selectedLut}>
                  <DropdownItem eventKey="1" onClick={this.props.linearClicked}>Linear</DropdownItem>
                  <DropdownItem eventKey="2" onClick={this.props.logarithmicClicked}>Logarithmic</DropdownItem>
                  <Dropdown.Divider />
                  <Container>
                    <FormCheck type="checkbox" onChange={this.props.autoscaleChecked} label="Autoscale" id="autoscale-check" checked={this.props.autoscaleCheckedBool} />
                    <FormCheck type="checkbox" onChange={this.props.temperatureChecked} label="Temperature color" id="temp-check" checked={this.props.temperatureCheckedBool} />
                  </Container>
                  <Dropdown.Divider />
                  <Container>
                    <Form.Group as={Row}>
                      <Form.Label column sm={3}>Min: </Form.Label>
                      <Col sm={9}>
                        <Form.Control type="number" value={this.props.min_max[0]} onChange={this.updateMin} />
                      </Col>
                    </Form.Group>
                    <Form.Group as={Row}>
                      <Form.Label column sm={3}>Max: </Form.Label>
                      <Col sm={9}>
                        <Form.Control type="number" value={this.props.min_max[1]} onChange={this.updateMax} />
                      </Col>
                    </Form.Group>
                    <ButtonGroup>
                      <Button onClick={this.saveMinMax}>Apply</Button>
                      <Button onClick={this.resetMinMax}>Reset</Button>
                    </ButtonGroup>
                  </Container>
                </DropdownButton>
              </OverlayTrigger>

              <OverlayTrigger placement="top" overlay={rotation}>
                <DropdownButton as={ButtonGroup} id='DropdownRotate' title={`Rotate (${this.props.rotation})`} disabled={!ready}>
                  <DropdownItem onClick={()=>{this.rotation(0)}} disabled={!ready}>None</DropdownItem>
                  <DropdownItem onClick={()=>{this.rotation(90)}} disabled={!ready}>Rotate 90</DropdownItem>
                  <DropdownItem onClick={()=>{this.rotation(180)}} disabled={!ready}>Rotate 180</DropdownItem>
                  <DropdownItem onClick={()=>{this.rotation(270)}} disabled={!ready}>Rotate 270</DropdownItem>

                  <Dropdown.Divider />

                  <Container>
                    <FormCheck type="checkbox" onChange={()=>this.flip(0)} label="Flip X" id="flip-x" checked={this.props.flip[0]} />
                    <FormCheck type="checkbox" onChange={()=>this.flip(1)} label="Flip Y" id="flip-y" checked={this.props.flip[1]} />
                  </Container>
                </DropdownButton>
              </OverlayTrigger>
            </ButtonGroup>

            <OverlayTrigger placement="top" overlay={profiles}>
              <Button onClick={this.toggleHideProfile.bind(this)} variant={this.props.profileSize.x?'primary':'secondary'}>Profiles</Button>
            </OverlayTrigger>

            <OverlayTrigger placement="top" overlay={profileType}>
              <ToggleButtonGroup type="radio" name="profileType" value={this.props.profileType} onChange={this.props.setProfileType} className="ml-1">
                <ToggleButton type="radio" value="beam">
                  Beam
                </ToggleButton>
                <ToggleButton type="radio" value="crosshair">
                  Crosshair
                </ToggleButton>
              </ToggleButtonGroup>
            </OverlayTrigger>
          </Col>
        </Row>

        <Row className="flex-nowrap">
          <Col className={classNames({'pr-0': this.props.profileSize.x})}>
            <Canvas id='canvas' />
          </Col>
          <Col className={classNames('pl-0', {'d-none': !this.props.profileSize.x})}>
            <div ref={this.graph2} style={{cursor:'pointer'}} onClick={this.toggleProfileSize.bind(this, 'y')}></div>
          </Col>
        </Row>
        <Row className={classNames({'d-none': !this.props.profileSize.x})}>
          <Col>
            <div ref={this.graph1} style={{cursor:'pointer'}} onClick={this.toggleProfileSize.bind(this, 'x')}></div>
          </Col>
          <Col></Col>
        </Row>
              
      </Container>
    );

  }

}



function mapStateToProps(state) {
  return {
    selectedLut:state.video.selectedLut,
    activeBkgnd: state.video.activeBkgnd,
    activeCrosshair: state.video.activeCrosshair,
    lockCrosshair: state.video.lockCrosshair,
    beam_markX: state.canvas.beam_markX,
    beam_markY: state.canvas.beam_markY,
    activeROI:state.video.activeROI,
    activeROIServer:state.video.activeROIServer,
    resetDesactivated: state.canvas.resetDesactivated,
    rotation: state.video.rotation,
    flip: state.video.flip,
    windowWidth:state.canvas.windowWidth,
    windowHeight:state.canvas.windowHeight,
    profileX : state.canvas.profileX,
    profileY : state.canvas.profileY,
    profileSize: state.canvas.profileSize,
    acqStatus: state.options.acqStatus,
    calib_x: state.options.calib_x,
    calib_y: state.options.calib_y,
    temperatureCheckedBool: state.video.temperatureCheckedBool,
    autoscaleCheckedBool: state.video.autoscaleCheckedBool,
    min_max: state.video.min_max,
    profileType: state.video.profileType,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    linearClicked: bindActionCreators(linearClicked, dispatch),
    logarithmicClicked: bindActionCreators(logarithmicClicked, dispatch),
    autoscaleChecked: bindActionCreators(autoscaleChecked, dispatch),
    temperatureChecked: bindActionCreators(temperatureChecked, dispatch),
    updateBackground: bindActionCreators(updateBackground,dispatch),
    setRoi: bindActionCreators(setRoi, dispatch),
    resetRoi: bindActionCreators(resetRoi, dispatch),
    setCrosshair: bindActionCreators(setCrosshair, dispatch),
    setRotation: bindActionCreators(setRotation,dispatch),
    setFlip: bindActionCreators(setFlip,dispatch),
    setImgDisplay:bindActionCreators(setImgDisplay,dispatch),
    getStatus:bindActionCreators(getStatus,dispatch),
    setProfileSize:bindActionCreators(profileSize,dispatch),
    updateDimensions:bindActionCreators(updateDimensions,dispatch),
    setBeamMark:bindActionCreators(setBeamMark,dispatch),
    setBeamMarkDone:bindActionCreators(setBeamMarkDone,dispatch),
    setLiveChecked:bindActionCreators(setLiveChecked,dispatch),
    setLockCrosshair:bindActionCreators(setLockCrosshair,dispatch),
    setOptions:bindActionCreators(setOptions,dispatch),
    updateMin:bindActionCreators(updateMin,dispatch),
    updateMax:bindActionCreators(updateMax,dispatch),
    setProfileType: bindActionCreators(setProfileType, dispatch),
  };
}

export default connect(mapStateToProps,mapDispatchToProps)(Video);
