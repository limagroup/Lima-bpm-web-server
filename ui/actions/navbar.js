import config from '../config';

let listPending = false
export function getList(){
  return (dispatch) => {
    if (listPending) return
    listPending = true

    fetch(config.base_url+'/list')
      .then((response) => {
        if(!response.ok){
          throw Error(response.statusText);
        }
        return response;
      }).then(response => response.json()) 
        .then(response => dispatch(getListDone(response)))
        .then(() => { listPending = false })
  } 
}

export function getListDone(data){
  return { type: 'GET_LIST_DONE', data }
}

export function setClientId(id) {
  return { type: 'SET_CLIENT_ID', id }
}