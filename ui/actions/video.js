import config from '../config'

import { setImgDisplay } from './options'

//////////////////////////////////////////BUTTON////////////////////////////////////////////////////////

export function linearClicked(){
  return (dispatch,getState) => {
    dispatch({ type: 'LINEAR_CLICKED'})
    const state = getState()
    dispatch(setOptions({ lut_method: state.video.selectedLut})) 
  }
}

export function logarithmicClicked(){
  return (dispatch,getState) => {
    dispatch({ type: 'LOGARITHMIC_CLICKED'})
    const state = getState()
    dispatch(setOptions({ lut_method: state.video.selectedLut})) 
  }
}

export function updateBackground(){

    return (dispatch,getState) => {
      const state = getState()
      fetch(config.base_url+'/'+state.bpmState.client_id+'/api/set_background?backgroundstate='+state.video.backgroundstate)
        .then((response) => {
          if(!response.ok){
            throw Error(response.statusText);
          }
          return response;
        }) .then(response => dispatch(backgroundState()))
           .catch(() => alert('Error, please check server console, updateBackground'))
    }
}

export function backgroundState(){
  return { type: 'BACKGROUND_STATE'}
}

export function setCrosshair(){
  return (dispatch,getState) => {
    const state = getState()
    return fetch(config.base_url+'/'+state.bpmState.client_id+'/api/lock_beam_mark?x='+state.canvas.beam_markX+'&y='+state.canvas.beam_markY)
      .then((response) => {
        if(!response.ok){
          throw Error(response.statusText);
        }
        return response;
      }) .then(response => dispatch(setCrosshairDone(state.canvas.beam_markX && state.canvas.beam_markY ? 1 : 0))) 
         .catch(() => alert('Problem to set BeamMark, check console server'))
  }

}

export function setCrosshairDone(state){
  return { type: 'SET_CROSSHAIR', state}
}

export function setRoi(){
  return { type: 'SET_ROI'}
}

export function setRotation(rotation){
    return (dispatch,getState) => {
      const state = getState()
      return fetch(config.base_url+'/'+state.bpmState.client_id+'/api/set_rotation?rotation='+rotation)
        .then((response) => {
          if(!response.ok){
            throw Error(response.statusText);
          }
          return response;
        }) .then(response => dispatch(setRotationDone(rotation)))
           .catch(() => alert('Error, please check server console, setRotation'))
    }
}

export function setRotationDone(rotation){
  return { type: 'ROTATION', rotation}
}

export function setFlip(flip){
    return (dispatch,getState) => {
      const state = getState()
      fetch(config.base_url+'/'+state.bpmState.client_id+'/api/set_flip?flipx='+(flip[0]?'1':'0')+'&flipy='+(flip[1]?'1':'0'))
        .then((response) => {
          if(!response.ok){
            throw Error(response.statusText);
          }
          return response;
        }) .then(response => dispatch(setFlipDone(flip)))
           .catch(() => alert('Error, please check server console, setFlip'))
    }
}

export function setFlipDone(flip){
  return { type: 'FLIP', flip}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////checkbox//////////////////////////////////////////////////////
export function liveChecked() { 
  return { type: "LIVE_CHECKED"}
}

export function autoscaleChecked(){
  return (dispatch,getState) => {
    dispatch({ type:"AUTOSCALE_CHECKED"})
    const state = getState()
    dispatch(setOptions({ autoscale: state.video.autoscaleCheckedBool}))
  }
}

export function temperatureChecked(){
  return (dispatch,getState) => {
    dispatch({ type:"TEMPERATURE_CHECKED"})
    const state = getState()
    dispatch(setOptions({ color_map: state.video.temperatureCheckedBool})) 
  } 
}
////////////////////////////////////////////////////////////////////////////////////////////////////////

export function setLockCrosshair(state) {
  return { type:"LOCK_CROSSHAIR", state}
}


export function setOptions(options) {
  return (dispatch,getState) => {
      const state = getState()
      return fetch(config.base_url+'/'+state.bpmState.client_id+'/api/set_options', {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: 'POST',
        body: JSON.stringify(options)
      })
        .then((response) => {
          if(!response.ok){
            throw Error(response.statusText);
          }
          return response;
        }) .then(() => {
          if (!state.options.liveCheckedBool) {
            dispatch(refreshImage())
          }
        })
  }
}

export function refreshImage(options) {
    return (dispatch,getState) => {
        const state = getState()
        return fetch(config.base_url+'/'+state.bpmState.client_id+'/api/refresh')
    }  
}

export function updateMin(value) {
  return { type: "UPDATE_MIN", value }
}

export function updateMax(value) {
  return { type: "UPDATE_MAX", value }
}

export function setProfileType(value) {
  return (dispatch, getState) => {
    const state = getState()
    dispatch({ type: "PROFILE_TYPE", value})
    dispatch(setOptions({ profile_type: value}))
  }
}
