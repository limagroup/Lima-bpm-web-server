const initialState = {
  client_id: null,
  bpmList: [],
  bpmListFetched: false,
};

export default function bpmState(state = initialState, action) {
  switch (action.type) {
    case 'SET_CLIENT_ID':
      return {
        ...state,
        client_id: action.id
      }

    case 'GET_LIST_DONE':
      return {
        ...state,
        bpmList: action.data.devices,
        bpmListFetched: true
      }

    default:
      return state
  }
}
