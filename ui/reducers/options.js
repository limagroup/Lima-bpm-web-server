const initialState = {
  exposureTimeValue: 0.01,
  samplingRateValue: 10,
  liveCheckedBool: 0,
  acqStatus: '',
  calib_x: 1,
  calib_y: 1,
  applyDisabled : false,
  saveDisabled : false,
  minExposureTime : 0,
  maxExposureTime : 0,
  minLatencyTime : 0,
  maxLatencyTime : 0,
  lockExposure: true,
};



export default function options(state = initialState, action) {
  
  switch (action.type) {
    
    case 'GET_STATUS_DONE':
    {
      return Object.assign({}, state, {minExposureTime:action.status.min_exposure_time, maxExposureTime:action.status.max_exposure_time, minLatencyTime:action.status.min_latency_time, maxLatencyTime:action.status.max_latency_time,liveCheckedBool:Number(action.status.live), exposureTimeValue:action.status.exposure_time, samplingRateValue:action.status.acq_rate , calib_x:action.status.calib_x, calib_y:action.status.calib_y, acqStatus: action.status.acq_status})
    }

    case 'LIVE_CHECKED':
    {
      if(state.liveCheckedBool == 0){
        return Object.assign({}, state, {liveCheckedBool : 1})
      }
      else{
        return Object.assign({}, state, {liveCheckedBool : 0})
      }

    }

    case 'SET_LIVE_CHECKED':
    {
        return Object.assign({}, state, {liveCheckedBool : action.state})
    }

    case 'SET_ACQ_STATUS':
    {
        return Object.assign({}, state, {acqStatus : action.state})
    }


    case 'TEXT_ENTER_EXPOSURE':
    {
      return Object.assign({}, state, {exposureTimeValue:action.text})
    }

    case 'TEXT_ENTER_SAMPLING':
    {
      return Object.assign({}, state, {samplingRateValue:action.text})
    }

    case 'TEXT_EMPTY_EXPOSURE':
    {
      return Object.assign({}, state, {exposureTimeValue:""})
    }

    case 'TEXT_EMPTY_SAMPLING':
    {
      alert("Acquisition rate can't be null or can't be lower than exposure time. Reminder : Hz = 1/s");
      return Object.assign({}, state, {samplingRateValue:""})
    }

    case 'TEXT_ENTER_X':
    {
      if(state.calib_y === ""){
        return Object.assign({}, state, {calib_x:action.text,applyDisabled:true,saveDisabled:true})
      }
      else{
        return Object.assign({}, state, {calib_x:action.text,applyDisabled:false,saveDisabled:false})
      }
    }

    case 'TEXT_EMPTY_X':
    {
      return Object.assign({}, state, {calib_x:"",applyDisabled:true,saveDisabled:true})
    }

    case 'TEXT_ENTER_Y':
    {
      if(state.calib_x === ""){
        return Object.assign({}, state, {calib_y:action.text,applyDisabled:true,saveDisabled:true})
      }
      else{
        return Object.assign({}, state, {calib_y:action.text,applyDisabled:false,saveDisabled:false})
      }

    }

    case 'TEXT_EMPTY_Y':
    {
      return Object.assign({}, state, {calib_y:"",applyDisabled:true,saveDisabled:true})
    }

    case 'LATENCY_NOT_IN_RANGE':
    {
      alert("You are trying to create a latency time ((1/AcquisitonRate)-ExposureTime) not in range with the specs of the camera, this value should be in range of ["+state.minLatencyTime+","+state.maxLatencyTime+"]"); 
      return Object.assign({}, state, {samplingRateValue:""});
    }

    case 'EXPOSURE_TIME_NOT_IN_RANGE':
    {
      alert("You are trying to create a exposure time not in range with the specs of the camera, this value should be in range of ["+state.minExposureTime+","+state.maxExposureTime+"]");
      return Object.assign({}, state, {exposureTimeValue:""});
    }

    case 'UPDATE_CALIBRATION_APPLY_DONE':
    {
       return Object.assign({}, state, {applyDisabled:true, saveDisabled:true, save:0})
    }

    case 'UPDATE_CALIBRATION_SAVE_DONE':
    {
       return Object.assign({}, state, {saveDisabled:true,save:1,calib_x:state.calib_x, calib_y:state.calib_y})
    }
    
    case 'LOCK_TOGGLE':
    {
        return Object.assign({}, state, {lockExposure : !state.lockExposure})
    }

    default:
      return state
  }


}
