const initialState = {
  selectedLut:"Linear",
  autoscaleCheckedBool: 0,
  temperatureCheckedBool:0,
  backgroundstate: 1,
  activeBkgnd:false,
  activeCrosshair: false,
  lockCrosshair: false,
  activeROI:false,
  activeROIServer:false,
  rotation: 0,
  flip: [false, false],
  min_max: [0, 65536],
  profileType: 'beam',
};



export default function video(state = initialState, action) {

  switch (action.type) {

    case 'GET_STATUS_DONE':
    {
      const crosshair = action.status.beam_mark_x && action.status.beam_mark_y;
      if(action.status.background===true){
        alert('A background is already set, you can reset it by clicking on the \'Bkgnd\' button')
        return Object.assign({}, state, {backgroundstate: 0, activeBkgnd:true, activeROIServer: action.status.roi, rotation: action.status.rotation, flip: action.status.flip, selectedLut: action.status.lut_method, temperatureCheckedBool: action.status.color_map, autoscaleCheckedBool: action.status.autoscale, activeCrosshair: crosshair, min_max: action.status.min_max,profileType: action.status.profile_type})
      } else {
        return Object.assign({}, state, { activeROIServer: action.status.roi, rotation: action.status.rotation, flip: action.status.flip, selectedLut: action.status.lut_method, temperatureCheckedBool: action.status.color_map, autoscaleCheckedBool: action.status.autoscale, activeCrosshair: crosshair, min_max: action.status.min_max, profileType: action.status.profile_type})
      }
    }

    case 'LINEAR_CLICKED':
    {
      return Object.assign({}, state, {selectedLut:"Linear"})
    }

    case 'LOGARITHMIC_CLICKED':
    {
      return Object.assign({}, state, {selectedLut:"Logarithmic"})
    }

    case 'AUTOSCALE_CHECKED':
    {
      if(state.autoscaleCheckedBool === 0){
        return Object.assign({}, state, {autoscaleCheckedBool : 1})
      }
      else{
        return Object.assign({}, state, {autoscaleCheckedBool : 0})
      }
    }

    case 'TEMPERATURE_CHECKED':
    {
      if(state.temperatureCheckedBool === 0){
        return Object.assign({}, state, {temperatureCheckedBool : 1})
      }
      else{
        return Object.assign({}, state, {temperatureCheckedBool : 0})
      }
    }

    case 'BACKGROUND_STATE':
    {
      if(state.backgroundstate === 1){
        return Object.assign({}, state, {backgroundstate: 0, activeBkgnd:true})
      }
      else{
        return Object.assign({}, state, {backgroundstate: 1, activeBkgnd:false})
      }
    }

    case 'SET_ROI':
    {
      if(state.activeROI === true){
        return Object.assign({}, state, {activeROI:false})
      }
      else{
        return Object.assign({}, state, {activeROI:true})
      }
    }

    case 'SET_ROI_DONE' :
    {
      return Object.assign({}, state, {activeROI:false})
    }

    case 'SET_CROSSHAIR':
    {
      return Object.assign({}, state, {activeCrosshair:action.state})
    }

    case 'ROTATION':
    {
      return Object.assign({}, state, {rotation: action.rotation})
    }

    case 'FLIP':
    {
      return Object.assign({}, state, {flip: action.flip})
    }

    case 'LOCK_CROSSHAIR':
    {
      return Object.assign({}, state, {lockCrosshair: action.state})
    }

    case 'UPDATE_MIN':
    {
      return Object.assign({}, state, {min_max: [action.value, state.min_max[1]]})
    }

    case 'UPDATE_MAX':
    {
      return Object.assign({}, state, {min_max: [state.min_max[0], action.value]})
    }

    case 'PROFILE_TYPE':
    {
      return Object.assign({}, state, { profileType: action.value })
    }

    default:
      return state
  }


}
