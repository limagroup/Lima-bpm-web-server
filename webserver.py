###########################################################################
# This file is part of LImA, a Library for Image Acquisition
#
#  Copyright (C) : 2009-2019
#  European Synchrotron Radiation Facility
#  CS40220 38043 Grenoble Cedex 9
#  FRANCE
#
#  Contact: lima@esrf.fr
#
#  This is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This software is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>.
############################################################################

import os
import time
import struct
import sys
import argparse
import socket
import json
import yaml

import numpy
import bottle
import PyTango

import gevent
from gevent.pywsgi import WSGIServer
from geventwebsocket import WebSocketError
from geventwebsocket.handler import WebSocketHandler

from bottle import response, request, abort


def is_float_str(v) -> bool:
    try:
        float(v)
    except ValueError:
        return False
    return True


class BVWebserver:
    def __init__(self, host, port, whitelist=None):
        self.host = host
        self.port = port
        self.app = bottle.Bottle()
        self.register_routes()
        self.event_counter = 0
        self.cameras_running = {}
        self.load_whitelist(whitelist)


    def watch_state(self, evt):
        if evt.attr_value:
            camera_name = self.find_camera(evt.attr_name)
            self.send_to_all(json.dumps({
                "camera_name": camera_name,
                "event": evt.attr_value.name,
                "value": evt.attr_value.value
            }))

    def watch_nb_frames(self, evt):
        if evt.attr_value:
            camera_name = self.find_camera(evt.attr_name)
            self.send_to_all(json.dumps({
                "camera_name": camera_name,
                "event": "video_live",
                "value": evt.attr_value.value == 0
            }))


    def camera_init(self, camera_name): 
        if camera_name not in self.cameras_running:
            self.event_counter = 0
            imgdisplayreply=None

            device = self.get_devices(camera_name=camera_name)
            limaccds_device = PyTango.DeviceProxy(device["lima"])

            try:
                limaccds_device.poll_attribute('acq_status', 500)
                limaccds_device.subscribe_event('acq_status', PyTango.EventType.CHANGE_EVENT, self.watch_state)

                for attr in ['acq_expo_time']:
                    attr_config = limaccds_device.get_attribute_config(attr)
                    valid_rel_change = is_float_str(attr_config.events.ch_event.rel_change)
                    valid_abs_change = is_float_str(attr_config.events.ch_event.abs_change)
                    if not valid_abs_change and not valid_rel_change:
                        attr_config.events.ch_event.rel_change = "0.0001"
                        limaccds_device.set_attribute_config(attr_config)

                    limaccds_device.poll_attribute(attr, 1000)
                    limaccds_device.subscribe_event(attr, PyTango.EventType.CHANGE_EVENT, self.watch_state)

            except PyTango.DevFailed as e:
                print('Cant connect to ', camera_name, e)
                return False
            try:
                bpm_device = PyTango.DeviceProxy(device["bpm"])
                self.cameras_running.update({camera_name : [limaccds_device, bpm_device, imgdisplayreply]})
            except AttributeError as e:
                print ("Be sure the Limaccds device is running and the bpm plugin is enabled")
                return False
                        
            bpm_device.subscribe_event('bvdata', PyTango.EventType.CHANGE_EVENT, self.decode_bvdata)

            return True

        else:
            try:
                if self.cameras_running[camera_name][1].State() == PyTango.DevState.UNKNOWN: #if the connection with device is lost (bpm stopped), we have to restart it.
                    self.cameras_running[camera_name][1].Start()

            except PyTango.DevFailed as e:
                print('Cant connect to ', camera_name, e)
                return False

            return True
            
    def find_camera(self, attr):
        """Find the correct camera personal_name from a device attr"""
        for camera_name, camera_info in self.cameras_running.items():
            if camera_info[0].name().lower() in attr.lower() or camera_info[1].name().lower() in attr.lower():
                return camera_name


    def send_to_all(self, message):
        to_rem = []
        if hasattr(self.server, 'clients'):
            for key, client in self.server.clients.items():
                try:
                    client.ws.send(message)

                except WebSocketError as e:
                    print('Websocket dead')
                    to_rem.append(key)

        for key in to_rem:
            del self.server.clients[key]


    def decode_bvdata(self, evt_bvdata):
        """Callback function from the subscribe_event on bvdata"""

        if self.event_counter == 0: # To avoid the first subscribe_event callback which result in an error
            print ("Subscribing to bvdata push event...")
        else:
            if not evt_bvdata.attr_value:
                print('No bvdata value returning')
                return

            if not evt_bvdata.attr_value.value:
                print('No bvdata value.value returning')
                return

            try:
                # print('bvdata')
                camera_name = self.find_camera(evt_bvdata.attr_name)
                self.parse_bvdata(camera_name, evt_bvdata.attr_value.value)

            except AttributeError as TypeError:
                print ("Subscribe event callback for ", self.cameras_running[camera_name][1])

    def parse_bvdata(self, camera_name, bvdata):
        if not camera_name in self.cameras_running:
            print("WARNING: Could not find a running camera:", camera_name)
            return

        bv_data = bvdata[1]
        HEADER_FORMAT = bvdata[0]
        (timestamp,framenb,
            X,Y,I,maxI,roi_top_x,roi_top_y,
            roi_size_getWidth,roi_size_getHeight,
            fwhm_x,fwhm_y, prof_x, prof_y, jpegData) = struct.unpack(HEADER_FORMAT, bv_data)

        profile_x = numpy.frombuffer(prof_x, dtype=numpy.int64).tolist()
        profile_y = numpy.frombuffer(prof_y, dtype=numpy.int64).tolist()

        self.cameras_running[camera_name][2] = {"framenb" : framenb, "X" : X, "Y" : Y, "I" : I, "fwhm_x" : fwhm_x, "fwhm_y" : fwhm_y,  "jpegData": jpegData.decode('utf-8'), "profile_x" : profile_x, "profile_y" : profile_y}
        self.send_to_all(json.dumps({
            "camera_name": camera_name,
            "event": "bvdata"
        }))


    def getExposuretime(self, camera_name):
        return self.cameras_running[camera_name][0].acq_expo_time

    def getAcqRate(self, camera_name):
        return (1.0/(self.cameras_running[camera_name][0].acq_expo_time+self.cameras_running[camera_name][0].latency_time))

    def setAcqRate(self, acqrate, exp_t, camera_name):
        new_latency = 1.0/acqrate - exp_t
        print("requested acq rate", acqrate, "calculated latency", new_latency, "old latency", self.cameras_running[camera_name][0].latency_time)
        if new_latency != self.cameras_running[camera_name][0].latency_time:
            self.cameras_running[camera_name][0].latency_time = new_latency
        print("set latency", self.cameras_running[camera_name][0].latency_time)

    def setExposuretime(self, exp_t, camera_name):
        print("requested exposure time", exp_t)
        self.cameras_running[camera_name][0].acq_expo_time = exp_t
        print("set exposure time", self.cameras_running[camera_name][0].acq_expo_time)

    def getAcqStatus(self, camera_name):
        return self.cameras_running[camera_name][0].acq_status

    def HasRoi(self, camera_name):
        img_dim = self.cameras_running[camera_name][0].image_roi
        img_width, img_height = self.getDimensionImage(camera_name)
        if img_dim[0]==0 and img_dim[1]==0 and img_dim[2]==img_width and img_dim[3]==img_height:
            return False
        else:
            return True
        
    def getDimensionImage(self, camera_name):
        return (self.cameras_running[camera_name][0].image_width,self.cameras_running[camera_name][0].image_height)


    def checkstatus(self, camera_name):
        if not (camera_name in self.cameras_running):
            return False

        try:
            state = self.cameras_running[camera_name][1].State()
        except PyTango.DevFailed as e:
            print('Cant get state of ', camera_name)
            return False

        if state == PyTango.DevState.UNKNOWN:
            return False

        return True


    ######################### CALLBACK FUNCTIONS TO APPLIED REQUEST #########################
    def getstatus(self, camera):
        if not self.camera_init(camera):
            return bottle.HTTPResponse(status=400, body={ "Could not connect to camera" })

        self.event_counter = 1
        reply = {
            "exposure_time": self.getExposuretime(camera),
            "acq_status": self.getAcqStatus(camera),
            "live": self.cameras_running[camera][0].acq_nb_frames == 0,
            "roi": self.HasRoi(camera),
            "full_width": self.getDimensionImage(camera)[0],
            "full_height": self.getDimensionImage(camera)[1],
            "acq_rate": self.getAcqRate(camera),
            "color_map": 1 if self.cameras_running[camera][1].color_map else 0,
            "autoscale": 1 if self.cameras_running[camera][1].autoscale else 0,
            "min_max": self.cameras_running[camera][1].min_max.tolist(),
            "lut_method": 'Logarithmic' if self.cameras_running[camera][1].lut_method == 'LOG' else 'Linear',
            "calib_x": self.cameras_running[camera][1].calibration[0],
            "calib_y":  self.cameras_running[camera][1].calibration[1],
            "background": self.cameras_running[camera][1].HasBackground(),
            "beam_mark_x": float(self.cameras_running[camera][1].beammark[0]),
            "beam_mark_y": float(self.cameras_running[camera][1].beammark[1]),
            "min_exposure_time": self.cameras_running[camera][0].valid_ranges[0],
            "max_exposure_time": self.cameras_running[camera][0].valid_ranges[1],
            "min_latency_time": self.cameras_running[camera][0].valid_ranges[2],
            "max_latency_time": self.cameras_running[camera][0].valid_ranges[3],
            "rotation": 0 if self.cameras_running[camera][0].image_rotation == 'NONE' else int(self.cameras_running[camera][0].image_rotation),
            "flip": (bool(self.cameras_running[camera][0].image_flip[0]), bool(self.cameras_running[camera][0].image_flip[1])),
            "profile_type": "beam" if self.cameras_running[camera][1].return_bpm_profiles else "crosshair"
        }
        return reply
        
    def setroi(self, camera):
        try:
            # calculate/get figures
            x_range = range(int(bottle.request.query.x), int(bottle.request.query.x+bottle.request.query.w))
            y_range = range(int(bottle.request.query.y), int(bottle.request.query.x+bottle.request.query.h))
            bm_x = int(self.cameras_running[camera][1].beammark[0])
            bm_y = int(self.cameras_running[camera][1].beammark[1])
            _, _, cam_xsize,cam_ysize = self.cameras_running[camera][0].image_sizes
            image_roi = self.cameras_running[camera][0].image_roi
            # now apply the new RoI
            self.cameras_running[camera][0].image_roi = (int(bottle.request.query.x),int(bottle.request.query.y), \
                                                         int(bottle.request.query.w),int(bottle.request.query.h))
            # check and update beammark if possible
            if int(bottle.request.query.w) == 0 and int(bottle.request.query.h) == 0:
                # RoI has been reset, so up scale beammark position
                bm_x = bm_x + image_roi[0]
                bm_y = bm_y + image_roi[1]
            else:                
                if bm_x in x_range and bm_y in y_range:
                    # new RoI, so down scale beammark position
                    bm_x = bm_x - int(bottle.request.query.x)
                    bm_y = bm_y - int(bottle.request.query.y)
                else:
                    # beammark out of window
                    bm_x = bm_y = 0
            self.cameras_running[camera][1].beammark = ([bm_x, bm_y])
            return { "message": "Set roi" }
        except Exception as e:
            print ("Could not set roi", e)
            # return  bottle.HTTPResponse(status=400, body={ "message": "Could not set roi" })
            

    def set_rotation(self, camera):
        try:
            self.cameras_running[camera][0].image_rotation = "NONE" if bottle.request.query.rotation == "0" else str(bottle.request.query.rotation)
        except:
            return bottle.HTTPResponse(status=400, body={ "Could not set rotation" })

    def set_flip(self, camera):
        try:
            self.cameras_running[camera][0].image_flip = (int(bottle.request.query.flipx), int(bottle.request.query.flipy))
        except:
            print ("Could not set flip")


    def set_options(self, camera):
        if "color_map" in bottle.request.json:
            self.cameras_running[camera][1].color_map = bool(int(bottle.request.json["color_map"]))
            return { "message": "color_map updated"}

        if "autoscale" in bottle.request.json:
            self.cameras_running[camera][1].autoscale = bool(int(bottle.request.json["autoscale"]))
            return { "message": "autoscale updated"}

        if "lut_method" in bottle.request.json:
            if bottle.request.json["lut_method"] == "Logarithmic":
                self.cameras_running[camera][1].lut_method = "LOG"
            else:
                self.cameras_running[camera][1].lut_method = "LINEAR"

            return { "message": "lut_method updated"}

        if "min_max" in bottle.request.json:
            mm = bottle.request.json["min_max"]
            self.cameras_running[camera][1].min_max = [int(mm[0]), int(mm[1])]
            return { "message": "min_max updated"}

        if "profile_type" in bottle.request.json:
            beam = bottle.request.json["profile_type"] == "beam"
            print("profile_type", bottle.request.json["profile_type"], beam)
            self.cameras_running[camera][1].return_bpm_profiles = beam
            return { "message": "profile_type set"}

    def refresh_image(self, camera):
        self.parse_bvdata(camera, self.cameras_running[camera][1].bvdata)

    def imgdisplay(self,camera):
        if not self.checkstatus(camera):
            return bottle.HTTPResponse(status=400, body={"message": "camera/bpm not running"})

        if self.getAcqStatus(camera) != 'Ready':
            return bottle.HTTPResponse(status=400, body={ "message": "cant modify parameters while an acquisition is in progres" })

        self.cameras_running[camera][1].calibration = ([float(bottle.request.query.calib_x), float(bottle.request.query.calib_y)])
        self.setAcqRate(float(bottle.request.query.acq_rate), float(bottle.request.query.exp_t), camera)
        self.setExposuretime(float(bottle.request.query.exp_t), camera)

        # reset acq. parameters to not depend on other application settings
        self.cameras_running[camera][0].saving_mode = 'manual'
        self.cameras_running[camera][0].acq_trigger_mode = 'internal_trigger'
        self.cameras_running[camera][0].acq_mode = 'single'

        if bool(int(bottle.request.query.live)):
            self.cameras_running[camera][0].acq_nb_frames = 0
        else:
            self.cameras_running[camera][0].acq_nb_frames = 1

        # always start first the bpm device
        if self.cameras_running[camera][1].State() != PyTango.DevState.ON:
            self.cameras_running[camera][1].Start()

        try:
            self.cameras_running[camera][0].prepareAcq()
            self.cameras_running[camera][0].startAcq()
        except Exception as e:
            print("could not set params and acquire:", e)
            return  bottle.HTTPResponse(status=503, body={ "message": "Could not set parameters and acquire" })

        return {}

    def stop_acq(self, camera):
        if not self.checkstatus(camera):
            return bottle.HTTPResponse(status=400, body={"message": "camera/bpm not running"})

        self.cameras_running[camera][0].stopAcq()
        self.cameras_running[camera][1].Stop()
        return { "message": "acquisition stopped"}

    def getimgdisplay(self, camera):
        if not self.checkstatus(camera):
            return bottle.HTTPResponse(status=400, body={"message": "camera/bpm not running"})

        reply = self.cameras_running[camera][2]
        if bottle.request.query.beammark_x != "undefined" and bottle.request.query.beammark_y != "undefined" and bottle.request.query.beammark_x != "" and bottle.request.query.beammark_y != "":
            I = self.cameras_running[camera][1].GetPixelIntensity([int(bottle.request.query.beammark_x), int(bottle.request.query.beammark_y)])
            reply.update({ "intensity": I })

        return reply
            
    def updatecalibration(self, camera):
        calib_x = float(bottle.request.query.calib_x)
        calib_y = float(bottle.request.query.calib_y)
        self.cameras_running[camera][1].calibration = ([calib_x, calib_y])
            
    def lockbeammark(self, camera):
        x = int(bottle.request.query.x) 
        y = int(bottle.request.query.y)
        self.cameras_running[camera][1].beammark = ([x, y])
            
    def getintensity(self, camera):
        x = int(bottle.request.query.x); y = int(bottle.request.query.y)
        reply = { "intensity": self.cameras_running[camera][1].GetPixelIntensity([x,y]) }
        return reply
            
    def setbackground(self, camera):
        if int(bottle.request.query.backgroundstate):
            if self.getAcqStatus(camera)=='Running':
                raise RuntimeError("Acquisition has not finished (or Live mode is on)")
            else:
                self.cameras_running[camera][1].TakeBackground()
        else:
            self.cameras_running[camera][1].ResetBackground()
                
######################### CALLBACK FUNCTIONS TO APPLIED REQUEST : END #########################



######################### BOTTLE ROUTES METHODS FOR BROSWER DISPLAY #########################
    def load_whitelist(self, whitelist):
        self.whitelist = []
        if whitelist:
            if os.path.exists(whitelist):            
                with open(whitelist) as stream:
                    try:
                        yml = yaml.safe_load(stream)
                        if yml:
                            self.whitelist = [camera.lower() for camera in yml.get('cameras', [])]
                        print("Whitelist enabled with the following cameras: ", self.whitelist)
                    except yaml.YAMLError as e: 
                        print("Whitelist yaml is malformed", e)

                return

        print("No whitelist loaded, allowing all cameras")


    def get_devices(self, camera_name=None):
        db = PyTango.Database()
        devices = db.get_server_list('LimaCCDs/*')

        dlist = []
        for d in devices:
            name = d.split('/')[-1]
            name = name.lower()
            if self.whitelist and not (name in self.whitelist):
                continue

            classes = db.get_device_class_list(d)
            cls_dict = dict(zip(*[reversed(classes)] * 2))
            bpm = cls_dict.get('BpmDeviceServer')

            if not bpm:
                continue

            d = {
                "lima": cls_dict.get('LimaCCDs'),
                "bpm": bpm,
                "name": name,
            }
            if name == camera_name:
                return d

            status = 1
            try:
                PyTango.DeviceProxy(bpm).ping()
            except:
                status = 0

            d["status"] = status
            dlist.append(d)

        return dlist

    def list(self):
        return { "devices": self.get_devices() }


    def index(self, camera=None):
        return bottle.static_file("index.html", root=os.path.dirname(os.path.abspath(__file__)))

    def serve_static(self, filename='bundle.js'):
        f = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'webpack_output/')
        return bottle.static_file(filename, root=f)

    def socket(self):
        wsock = request.environ.get('wsgi.websocket')
        if not wsock:
            abort(400, 'Expected WebSocket request.')

        while True:
            try:
                gevent.sleep(0.1)

            except WebSocketError:
                break


######################### BOTTLE ROUTES METHODS FOR BROSWER DISPLAY : END #########################


    def register_routes(self):
        self.app.route('/', callback=self.options_handler, method='OPTIONS')
        self.app.route('/<path:path>', callback=self.options_handler, method='OPTIONS')

        self.app.hook('before_request')(self.enable_cors)

        self.app.route('/', callback=self.index)
        self.app.route('/<camera>', callback=self.index)
        self.app.route('/webpack_output/<filename>', callback=self.serve_static)
        self.app.route('/list', callback=self.list)
        self.app.route('/socket', callback=self.socket)

        self.app.route('/<camera>/api/get_status', callback=self.getstatus)
        self.app.route('/<camera>/api/set_roi', callback=self.setroi)
        self.app.route('/<camera>/api/set_rotation', callback=self.set_rotation)
        self.app.route('/<camera>/api/set_flip', callback=self.set_flip)
        self.app.route('/<camera>/api/set_options', callback=self.set_options, method="POST")
        self.app.route('/<camera>/api/img_display_config', callback=self.imgdisplay)
        self.app.route('/<camera>/api/img_display', callback=self.getimgdisplay)
        self.app.route('/<camera>/api/refresh', callback=self.refresh_image)
        self.app.route('/<camera>/api/stop', callback=self.stop_acq)
        self.app.route('/<camera>/api/update_calibration', callback=self.updatecalibration)
        self.app.route('/<camera>/api/lock_beam_mark', callback=self.lockbeammark)
        self.app.route('/<camera>/api/get_intensity', callback=self.getintensity)
        self.app.route('/<camera>/api/set_background', callback=self.setbackground)


    def enable_cors(self):
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
        response.headers['Access-Control-Allow-Headers'] = 'Authorization, Origin, Accept, Content-Type, X-Requested-With'

    def options_handler(self, path=None):
        return


    def run_forever(self):
        try:
            self.server = WSGIServer((self.host, self.port), self.app, handler_class=WebSocketHandler)
            self.server.serve_forever()
        except KeyboardInterrupt:
            print ("Stopping Webserver.")
            sys.exit()


 ########--------------------------------------------------------------------------------------########

def main():    
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-b', '--bind', help='IP/Hostname of interface to bind to', default=socket.gethostname())
    parser.add_argument('-p', '--port', help='The port to bind to', default=8066, type=int, metavar='Port')
    parser.add_argument('-w', '--whitelist', help='Whitelist to load', metavar='Whitelist')

    args = parser.parse_args()

    print(f"BPM Viewer running on http://{args.bind}:{args.port}")
    BVWebserver(args.bind, args.port, whitelist=args.whitelist).run_forever()


if __name__ == "__main__":
    main()
