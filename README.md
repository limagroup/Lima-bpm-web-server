# LimaBpmWebServer

[![License](https://img.shields.io/github/license/esrf-bliss/lima.svg?style=flat)](https://opensource.org/licenses/GPL-3.0)
[![Gitter](https://img.shields.io/gitter/room/esrf-bliss/lima.svg?style=flat)](https://gitter.im/esrf-bliss/LImA)
[![Conda](https://img.shields.io/conda/dn/esrf-bcu/lima-bpm-web-server.svg?style=flat)](https://anaconda.org/esrf-bcu)
[![Version](https://img.shields.io/conda/vn/esrf-bcu/lima-bpm-web-server.svg?style=flat)](https://anaconda.org/esrf-bcu)
[![Platform](https://img.shields.io/conda/pn/esrf-bcu/lima-bpm-web-server.svg?style=flat)](https://anaconda.org/esrf-bcu)


## Lima Beam Position Monitoring Web Server

**LimaBpmWebServer** is a python web-server which can be used to easily display
live images from cameras driven by a LimaCCDs tango server. In addition the
web-server can display some information like the position and the size of a
unique beam spot. The beam spot position is automatically calculated by the Bpm
plugin device of the LimaCCDs tango server.

Be sure you have a recent LimaCCDs server installation for your cameras (version
1.9.2 or upper).

## Web interface

![LimaBpmWebServer on firefox](doc/LimaBpmWebServer-1.png)

## Install from conda package

A Conda package is available on Anaconda forge from channel `esrf-bcu`:
```bash
conda install -c conda-forge -c esrf-bcu -c tango-controls lima-bpm-web-server
```

## Build & Install from source

The server is written in Python and JavaScript and we use webpack to bundle the
JavaScript files. In addition to the project source code one should install
some dependencies to build a new webpack bundle and to run the server.

### Get the source from github.com

git clone https://github.com/esrf-bliss/Lima-bpm-web-server


### Install dependencies

**Python**

- bottle >= 0.12.17
- gevent >= 1.4.0
- PyTango >= 9.3.0
- gevent-websocket >= 0.10.1
- pyyaml => 5.1.1

**JavaScript**

- node >= 6.14.2
- npm >= 6.1.0

### build webpack bundle

Before running the webpack bundler you should install some JavaScript packages
listed in package.json, simply run:
```bash
npm install
```

Bundle the JavaScript code:
```bash
npx webpack --config webpack.config.js"
```

### Install server

Use `cmake` to install:
```bash
cmake -B build -H.
cmake --build build --target install
```

## Run the server

To run the server, just execute:
```bash
LimaBpmWebServer
```

The server accepts some optional parameters:
```bash
LimaBpmWebServer  --help
usage: LimaBpmWebServer [-h] [-b BIND] [-p Port] [-w Whitelist]

optional arguments:
  -h, --help            show this help message and exit
  -b BIND, --bind BIND  IP/Hostname of interface to bind to (default:
                        <hostname>)
  -p Port, --port Port  The port to bind to (default: 8066)
  -w Whitelist, --whitelist Whitelist
                        A Whitelist yml file to load (default: None)
```

The white-list YAML file can be used to provide a short list and limit the web
access to a certain number of cameras.

For an example, have a look on the example file `whitelist-example.yml`:
```yaml
cameras:
  - bcu_basler
  - bcu_basler_color
```

Without a white-list file the server will give access to
all the cameras it can find via the Tango database.

Start a browser pointing to [localhost:8066](http://localhost:8066)
